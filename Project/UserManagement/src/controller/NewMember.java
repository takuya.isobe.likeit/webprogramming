package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.userDAO;
import model.User;

/**
 * Servlet implementation class NewMember
 */
@WebServlet("/NewMember")
public class NewMember extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewMember() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewMember.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//各name付けされた記入欄の数値を受け取り変数へ入れる
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birthday = request.getParameter("birthday");

		//同じloginIdが登録されていないかを確認
		//あれば数値、なければnullが返ってくる
		userDAO userDAO = new userDAO();
		try {
			String password00 = userDAO.password00(password);

		User user = userDAO.findLoginId(loginId);

		//if文にてパスワードとパスワード確認を比較し違う場合は、エラー表記
		//パスワードとパスワード確認の入力が違った場合、loginId他が空欄だった場合エラーメッセージを出し
		//NewMember.jspにフォワード
		if(!password.equals(password2)||loginId.equals("")||password.equals("")||name.equals("")
				||birthday.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// NewMember.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewMember.jsp");
			dispatcher.forward(request, response);
			return;
		//上記if文に該当しなかった(false)だった場合、loginIdを確認、nullの場合入力数値を登録
		}else if(user!=null){
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/NewMember.jsp");
			dispatcher.forward(request, response);
			return;
		}else {

		userDAO.Register(loginId,name,birthday,password00);
		} }catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserList");
	}
}


















