package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.userDAO;
import model.User;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		userDAO userDao = new userDAO();
		try {
			HttpSession session = request.getSession();
			User userINFO = (User)session.getAttribute("userInfo");
		if(userINFO !=null) {
			List<User> userList = userDao.findAll();
			// リクエストスコープにユーザ一覧情報をセット
			session.setAttribute("userList", userList);
			// ユーザ一覧のjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/alluser.jsp");
			dispatcher.forward(request, response);
			} else {
		//このサーブレットを起動するとindex.jspが起動し遷移するフォワード文
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
		dispatcher.forward(request, response);
		  }
		}catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		 request.setCharacterEncoding("UTF-8");
		 	//index.jspで記入された数値を受け取る文
		 	String loginId = request.getParameter("loginId");
		 	//request.getParameterで入力欄の文字列を取得する
		 	//()内ダブルコーテーションで囲まれた文字列でname = loginIdと名前付けされた記入欄に記入された
		 	//文字を受け取り変数に入れている
			String password = request.getParameter("password");

			// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
			userDAO userDAO = new userDAO();
			try {
				String password00 = userDAO.password00(password);

			User user = userDAO.findByLoginInfo(loginId, password00);
			//userDAOのfindByLoginInfoを起動、()内の引数を渡す
			/** テーブルに該当のデータが見つからなかった場合 **/
			if (user == null) {
				// リクエストスコープにエラーメッセージをセット
				request.setAttribute("errMsg", "ログインに失敗しました。");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
				dispatcher.forward(request, response);
				return;

			}

			/** テーブルに該当のデータが見つかった場合 **/
			// セッションにユーザの情報をセット
			HttpSession session = request.getSession();
			//(箱.数値の入った変数)箱をjspに渡しuser.loginIdでログインIDをjsp内に表記
			session.setAttribute("userInfo", user);

			// ユーザ一覧のサーブレットにリダイレクト
			response.sendRedirect("UserList");
			} catch (NoSuchAlgorithmException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}

	}





















