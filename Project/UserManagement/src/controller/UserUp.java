package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.userDAO;
import model.User;

/**
 * Servlet implementation class UserUp
 */
@WebServlet("/UserUp")
public class UserUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUp() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//リクエストパラメーターでIDを取得
				String id = request.getParameter("id");


				//idを使って検索
				userDAO userDao = new userDAO();
				User userInfo = userDao.find(id);

				// リクエストスコープに情報をセットs
				request.setAttribute("userUp", userInfo);

				//jspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUp.jsp");
				dispatcher.forward(request, response);
			}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String id = request.getParameter("id");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String password2 = request.getParameter("password2");
		String birthday = request.getParameter("birthday");


		//if文にてパスワードとパスワード確認を比較し違う場合は、エラー表記
		//パスワードとパスワード確認の入力が違った場合、記入欄が空欄だった場合エラーメッセージを出し
		//userUp.jspにフォワード
		if(!password.equals(password2)||name.equals("")||birthday.equals("")) {
			request.setAttribute("errMsg", "入力された内容は正しくありません。");
			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUp.jsp");
			dispatcher.forward(request, response);
			return;}else {
		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDAO userDAO = new userDAO();
		try {
			String password00 = userDAO.password00(password);

		userDAO.up(id,password00,name,birthday);
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		// ユーザ一覧のサーブレットにリダイレクト
		response.sendRedirect("UserList");
		}
	}

}
