package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * Servlet implementation class userDAO
 */
public class userDAO {

 public User findByLoginInfo(String loginId, String password) {
   Connection conn = null;
   try {
	   conn = DBM.getConnection();
	   //sqlのSELECT分を用意
	   String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";
	// SELECTを実行し、結果表を取得
       PreparedStatement pStmt = conn.prepareStatement(sql);
       pStmt.setString(1, loginId);//左から1番目の?にLoginサーブレットからきた文字が入る
       pStmt.setString(2, password);
       ResultSet rs = pStmt.executeQuery();

       if (!rs.next()) {
           return null;
       }
       String loginIdData = rs.getString("login_id");
       //SQLから受け取った文字が()内に入っている、()内はカラム名を記入
       String nameData = rs.getString("name");
       return new User(loginIdData, nameData);
   } catch (SQLException e) {
       e.printStackTrace();
       return null;
   	} finally {
   	 // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            	}
        	}
   		}
 	}

public List<User> findAll() {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
        // データベースへ接続
        conn = DBM.getConnection();

        // SELECT文を準備
        String sql = "SELECT * FROM user where id > 1";

         // SELECTを実行し、結果表を取得
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery(sql);

        // 結果表に格納されたレコードの内容を
        // Userインスタンスに設定し、ArrayListインスタンスに追加
        while (rs.next()) {
            int id = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");
            User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

            userList.add(user);
        }
    } catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return userList;
	}
public void Register(String loginId, String name,String birthday,String password)  {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();


    try {

        // データベースへ接続
        conn = DBM.getConnection();

        // INSERT文を準備
        String sql =  "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date ) VALUES (?,?,?,?,now(),now())";

        // INSERTを実行
        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1, loginId);
		stmt.setString(2, name);
		stmt.setString(3, birthday);
		stmt.setString(4, password);
        stmt.executeUpdate();

    } catch (SQLException e) {
        e.printStackTrace();

    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
    }
}
public User find(String id) {
	  Connection conn = null;
    try {
        // データベースへ接続
        conn = DBM.getConnection();
        // SELECT文を準備
        String sql = "SELECT *  FROM user WHERE ID = ?";

       // 結果表に格納されたレコードの内容を
       // Userインスタンスに設定し、ArrayListインスタンスに追加
    	PreparedStatement pst = conn.prepareStatement(sql);
        pst.setString(1, id);
        ResultSet rs = pst.executeQuery();

        if (!rs.next()) {
            return null;
        }
        int id1 = rs.getInt("id");
        String loginIdData = rs.getString("login_id");
        String nameData = rs.getString("name");
        Date birthDate = rs.getDate("birth_date");
        String password = rs.getString("password");
        String createDate = rs.getString("create_date");
        String updateDate = rs.getString("update_date");
        return new User(id1,loginIdData, nameData,birthDate,password,createDate,updateDate);

} catch (SQLException e) {
    e.printStackTrace();
} finally {
    // データベース切断
    if (conn != null) {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
      }
	}
	return null;
  }
public void up(String ID,String password,String name,String birthday) {
    Connection conn = null;
    List<User> userList = new ArrayList<User>();

    try {
        // データベースへ接続
        conn = DBM.getConnection();

        // INSERT文を準備
        // TODO: 未実装：管理者以外を取得するようSQLを変更する
        String sql =  "UPDATE user SET password = ? ,  name = ? , birth_date = ? WHERE ID = ?";

        // UPDATEを実行
        PreparedStatement stmt = conn.prepareStatement(sql);
		stmt.setString(1, password);
		stmt.setString(2, name);
		stmt.setString(3, birthday);
		stmt.setString(4,ID);
        stmt.executeUpdate();

    } catch (SQLException e) {
        e.printStackTrace();

    } finally {
        // データベース切断
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();

            }
        }
    }
}
public void deleat (String ID) {
	Connection conn = null;
	try {
        // データベースへ接続
        conn = DBM.getConnection();
        String sql = " DELETE FROM user WHERE ID = ?";

        PreparedStatement stmt = conn.prepareStatement(sql);
        stmt.setString(1,ID);
        stmt.executeUpdate();
	 } catch (SQLException e) {
	        e.printStackTrace();

	    } finally {
	        // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	 }
	}
  }
}
public User findLoginId(String loginId) {
	   Connection conn = null;
	   try {
		   conn  = DBM.getConnection();
		   //sqlのSELECT分を用意
		   String sql = "SELECT login_id FROM user WHERE login_id = ?";
		// SELECTを実行し、結果表を取得
	       PreparedStatement pStmt = conn.prepareStatement(sql);
	       pStmt.setString(1, loginId);
	       ResultSet rs = pStmt.executeQuery();

	       if (!rs.next()) {
	           return null;
	       }
	       String loginIdData = rs.getString("login_id");
	       return new User(loginIdData);
	   } catch (SQLException e) {
	       e.printStackTrace();
	       return null;
	   	} finally {
	   	 // データベース切断
	        if (conn != null) {
	            try {
	                conn.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            	}
	        	}
	   		}
	 	}

public String password00(String password) throws NoSuchAlgorithmException {
	//ハッシュを生成したい元の文字列
	String source = password;
	//ハッシュ生成前にバイト配列に置き換える際のCharset
	Charset charset = StandardCharsets.UTF_8;
	//ハッシュアルゴリズム
	String algorithm = "MD5";

	//ハッシュ生成処理
	byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	String result = DatatypeConverter.printHexBinary(bytes);
	return result;

	}
public List<User> searchs(String name) {
	 Connection conn = null;
	 List<User> userList = new ArrayList<User>();
     try {
         // データベースへ接続
    	 conn  = DBM.getConnection();

         // SELECT文を準備
         String sql = "SELECT * FROM user WHERE name LIKE'%?%'";

         //SELECT * FROM user WHERE login_id = 'abc';
         //select*from user where  birth_date> '1995-01-01' and birth_date< '2000-12-30';
          // SELECTを実行し、結果表を取得

         // SELECTを実行し、結果表を取得
         Statement stmt = conn.createStatement();
         ResultSet rs = stmt.executeQuery(sql);

         // 結果表に格納されたレコードの内容を
         // Userインスタンスに設定し、ArrayListインスタンスに追加
         while (rs.next()) {
        	// String loginId = rs.getString("login_id");
             String Name = rs.getString("name");
             //Date birthDate = rs.getDate("birth_date");
             User user = new User(Name);


             userList.add(user);
         }
     } catch (SQLException e) {
         e.printStackTrace();
         return null;
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
                 return null;
             }
         }
     }
     return userList;
     }

public List<User> search(String loginID,String name,String birthdayA,String birthdayB) {
	 Connection conn = null;
	 List<User> userList = new ArrayList<User>();
	   try {
		   conn  = DBM.getConnection();
		   //sqlのSELECT分を用意
		   String sql = "SELECT * FROM user WHERE id > 1";
		   	if(!(loginID.equals(""))) {
		   		sql += " and login_id ='"+ loginID +"'";
		   	}
		    if(!(name.equals("") )) {
		   		sql += " and name LIKE '%" + name + "%'";
		   	}
		    if(!(birthdayA.equals("") )) {
		   		sql += " and  birth_date BETWEEN'"+ birthdayA+ "'and'" +birthdayB+"'" ;
		   	}

		// SELECTを実行し、結果表を取得
		   Statement stmt = conn.createStatement();
	        ResultSet rs = stmt.executeQuery(sql);

	       while (rs.next()) {
               int id = rs.getInt("id");
               String loginId = rs.getString("login_id");
               String Name = rs.getString("name");
               Date birthDate = rs.getDate("birth_date");
               String password = rs.getString("password");
               String createDate = rs.getString("create_date");
               String updateDate = rs.getString("update_date");
               User user = new User(id, loginId, Name, birthDate, password, createDate, updateDate);

               userList.add(user);
           }
       } catch (SQLException e) {
           e.printStackTrace();
           return null;
       } finally {
           // データベース切断
           if (conn != null) {
               try {
                   conn.close();
               } catch (SQLException e) {
                   e.printStackTrace();
                   return null;
               }
           }
       }
       return userList;
}
}

