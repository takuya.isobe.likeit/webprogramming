<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
 <div style=
         "padding: 20px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          width:600px;
          background-color: darkgray;
          color: white;">
 <ul class="nav navbar-nav navbar-right">

 		<div class= "text-right">
            <li class="navbar-text">${userInfo.name} さん </li>

  			<li class="dropdown">
  			  <a href="Logout" class="navbar-link logout-link"style="color: red;">ログアウト</a>
            </li>
        </div>

  <div class=""></div>
    </div>
    <div style=
         "padding: 20px;
          margin-bottom: 1px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          width:600px;">
    <div class = "hyousi">

        <h1>
        <p class="text-center">
            <b>ユーザー新規登録</b>
        </p>
        </h1>
        <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<form class="form-signin" action="NewMember" method="post">
     <div class="form-loginID row">
        <label for="inputPassword"
               class="col-sm-0 col-form-label"
               ><b>ログインID</b>
        </label>
        <div class="col-sm-10"
             align="right">
        <input type="text"
        	   name="loginId"
               class="form-control"
               id="inputPassword"
               style = width:250px;>
        </div>
    </div>

        <p></p>

        <div class="form-username row">
    <label for="inputPassword"
           class="col-sm-0 col-form-label"><b>パスワード</b>
    </label>
        <div class="col-sm-10"
             align="right">
        <input type="password"
        	   name="password"
               class="form-control"
               id="inputPassword"
               style = width:250px;
               >
        </div>
        </div>
        <p></p>

     <div class="form-loginID row">
        <label for="inputPassword"
               class="col-sm-0 col-form-label"><b>パスワード（確認）　　</b>
        </label>
        <div class="col-sm-8"
             align="right">
        <input type="password"
        	   name="password2"
               class="form-control"
               id="inputPassword"
               style = width:250px;>
        </div>
    </div>

        <p></p>

        <div class="form-username row">
    <label for="inputPassword"
           class="col-sm-0 col-form-label"><b>ユーザ名　</b>
    </label>
        <div class="col-sm-10"
             align="right">
        <input type="text"
        	   name="name"
               class="form-control"
               id="inputPassword"
               style = width:250px;>
        </div>
        </div>
                <p></p>

        <div class="form-username row">
    <label for="inputPassword"
           class="col-sm-0 col-form-label"
           ><b>生年月日　</b>
    </label>
        <div class="col-sm-10"
             align="right">
        <input type="date"
        	   name="birthday"
               class="form-control"
               id="inputPassword"
               style = width:250px;>
        </div>
        </div>
        <p></p>
             <p class="text-center">
  <button type="submit" class="btn btn-secondary" >　登録　</button>
    </p>
    </form>
    <div align="left">

            <a href="UserList">戻る</a>
            </div>
        </div>
</body>
</html>