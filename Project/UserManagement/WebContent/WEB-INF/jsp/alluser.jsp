<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<head>
<meta charset="UTF-8">

    <title>ユーザー覧</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="all%20user.css" rel="stylesheet">
</head>
<body>
    <div style=
         "padding: 20px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          width:700px;
          background-color: darkgray;
          color: white;">

 <ul class="nav navbar-nav navbar-right">

 		<div class= "text-right">
            <li class="navbar-text">${userInfo.name} さん </li>

  			<li class="dropdown">
  			  <a href="Logout" class="navbar-link logout-link"style="color: red;">ログアウト</a>
            </li>
        </div>
       <div class=""></div>
    </div>
    <div style=
         "padding: 20px;
          margin-bottom: 1px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          width:700px;">
    <div class = "hyousi">
    <h1>
         <p class="text-center">
        ユーザー覧
        </p>
    </h1>
    </div>
	<form class="form-signin" action="NewMember" method="post">
        <div align="right">
            <a href = "NewMember">新規登録</a>
        </div>
	</form>
	<form class="form-signin" action="UserList" method="post">
     <div class="form-loginID row">
        <label for="inputPassword"
               class="col-sm-0 col-form-label"><b>ログインID</b>
        </label>
        <div class="col-sm-10"
             align="center">

        <input type="text"
        	   name="loginId"
               class="form-control"
               id="inputPassword"
               style="width : 400px;"
               >
        </div>
    </div>

        <p></p>

        <div class="form-username row">
    <label for="inputPassword"
           class="col-sm-0 col-form-label"><b>ユーザー名</b>
    </label>
        <div class="col-sm-10"
             align="center">
        <input type="text"
			   name="name"
               class="form-control" id="inputPassword"
               style="width:400px;">
        </div>

    </div>
        <p></p>
        <div class="form-username row">
    <label for="inputPassword"
           class="col-sm-0 col-form-label"><b>生年月日　　　　　</b>
    </label>
        <div class="col-sm-4"
             >
            <input type="date"
            	name="birthdayA"
               class="form-control"
               style = width:160px;>
            </div>
            <label><h2>~</h2></label>
         <div class="col-sm-4">
        <input type="date"
               class="form-control" name="birthdayB"
               style = width:160px;>
        </div>
        </div>
        <p class="text-right">
            <button type="submit" class="btn btn-secondary" >　　検索　　</button>
        <div style=
         "padding: px;
          border: 1px solid #333233;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          margin-bottom:50px;
          width:400px;

          ">
          </div>
</form>

    <table
           border="1"
           width="450"
           >
<table border width="600"
       align="center">

<tr align="center">
<th bgcolor="#d3d3d3">ログインID</th>
<th bgcolor="#d3d3d3">ユーザー名</th>
<th bgcolor="#d3d3d3">生年月日</th>
<th bgcolor="#d3d3d3">　　　</th>
</tr>

<c:forEach var="user" items="${userList}">

<tr>
<td>${user.loginId}</td>
<td>${user.name}</td>
<td>${user.birthDate}</td>
<td align="center">

<c:choose>
    <c:when test = "${userInfo.loginId.equals('admin')}">
<a
             class="btn btn-primary" href="UserInfo?id=${user.id}">
             詳細</a>
    <a
            class="btn btn-success"href="UserUp?id=${user.id}">
            更新</a>
    <a type="submit"
            class="btn btn-danger"href="UserDleate?id=${user.id}">
            削除</a>
    </c:when>


    		<c:when test="${userInfo.loginId.equals(user.loginId)}">
    <a class="btn btn-primary" href="UserInfo?id=${user.id}">
             詳細</a>
              <a class="btn btn-success"href="UserUp?id=${user.id}">
            更新</a>
             </c:when>

    <c:otherwise>
    <a class="btn btn-primary" href="UserInfo?id=${user.id}">
             詳細</a>
    </c:otherwise>

</c:choose>
</td>
</tr>
</c:forEach>


</table>

  </table>

    </div>


</body>
</html>