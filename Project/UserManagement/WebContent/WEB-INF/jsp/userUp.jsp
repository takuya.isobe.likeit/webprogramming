<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">

    <title>ユーザー覧</title>
     <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="all%20user.css" rel="stylesheet">
</head>
<body>
    <div style=
         "padding: 20px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          width:500px;
          background-color: darkgray;
          color: white;">
 <ul class="nav navbar-nav navbar-right">

 		<div class= "text-right">
             <li class="navbar-text">${userInfo.name} さん </li>

  			<li class="dropdown">
  			  <a href="Logout" class="navbar-link logout-link"style="color: red;">ログアウト</a>
            </li>
        </div>
</ul>
       <div class=""></div>
    </div>
    <div style=
         "padding: 20px;
          margin-bottom: 1px;
          border: 1px solid #333333;
          margin-left:auto;
          margin-op:auto;
          margin-right:auto;
          width:500px;">

                <h1>
        <p class="text-center">
            <b>ユーザ情報更新</b>
        </p>
        </h1>
    <c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
<form class="form-signin" action="UserUp" method="post">
<input name = "id" value=${userUp.id}} type = "hidden">
<table  width="450">
    <tr align="center">
      <th>ログインID</th>
      <th >　　　　　　　　　　${userUp.loginId}
        </th>
    </tr>
</table>

<p></p>

<table  width="450">
    <tr align="center">
      <th>パスワード　</th>
      <th >　　　　　<input type="password"name="password"></th>
    </tr>
</table>

<p></p>

<table  width="600">
    <tr>
      <th>パスワード（確認）</th>
        <th></th>
      <th ><input type="password"name="password2">　　　</th>
    </tr>
</table>

<p></p>

<table  width="450">
    <tr align="center">
      <th>ユーザ名　　</th>
      <th >　　　　　<input type="text" name="name" value=${userUp.name}></th>
    </tr>
</table>

<p></p>

<table  width="450">
    <tr align="center">
      <th >生年月日　　</th>
      <th >　　　　　<input type="date" name="birthday" value=${userUp.birthDate}></th>
    </tr>
</table>

<p></p>
 <p class="text-center">
  <button type="submit" class="btn btn-secondary" >　更新　</button>
    </p>
   </form>
 <div align="left">

            <a href="UserList">戻る</a>
            </div>
    </div>
</body>
</html>